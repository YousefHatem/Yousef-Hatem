<h1 align="center">Hi, I'm Yousef 👋</h1>
<p align="center">
    <a href="https://twitter.com/Yousef26Hatem"><img src="https://img.shields.io/badge/twitter-%231FA1F1?style=flat&logo=twitter&logoColor=white"/></a>
    <a href="https://www.linkedin.com/in/yousefhatem"><img src="https://img.shields.io/badge/linkedin-%230177B5?style=flat&logo=linkedin&logoColor=white"/></a>
    <a href="https://www.facebook.com/Yousef26Hatem"><img src="https://img.shields.io/badge/facebook-%230d8cf0?style=flat&logo=facebook&logoColor=white"/></a>
    <a href="https://t.me/Yousef26Hatem"><img src="https://img.shields.io/badge/telegram-%2329aaed?style=flat&logo=telegram&logoColor=white"/></a>
  </p>  
  
  <img src="https://gitlab.com/Yousef-Hatem/Yousef-Hatem/raw/main/profile-img.png" align="right" width="25%"/>

I'm a software developer who is passionate about creating technology to elevate people and Some technologies I enjoy working with include Javascript, Angular, and PHP, Laravel.

- 🔍 I have a package on npm [@validation-all](https://www.npmjs.com/package/validation-all)
- 🔭 I'm a software developer [@AbuDiyab](https://abudiyab-soft.com)
- 🤖 I programmed a trading bots (Automated Trading)
- 💬 Ask me about **Frontend and Backand**

### 🛠 Tech Stack

![Javascript](http://img.shields.io/badge/-Javascript-fcd400?style=flat-square&logo=javascript&logoColor=black)
![Typescript](http://img.shields.io/badge/-Typescript-3178c6?style=flat-square&logo=typescript&logoColor=white)
![Angular](https://img.shields.io/badge/-Angular-dd0031?style=flat-square&logo=angular&logoColor=white)
![Php](http://img.shields.io/badge/-Php-767bb3?style=flat-square&logo=php&logoColor=white)
![Laravel](https://img.shields.io/badge/-Laravel-ff2d20?style=flat-square&logo=Laravel&logoColor=white)
![Sql](http://img.shields.io/badge/-Sql-00758f?style=flat-square&logo=Mysql&logoColor=white)
![Java](http://img.shields.io/badge/-Java-e8892f?style=flat-square&logo=java&logoColor=white)
![C++](https://img.shields.io/badge/-C++-005697?style=flat-square&logo=cplusplus&logoColor=white)
![Android Studio](https://img.shields.io/badge/-Android%20Studio-00de7a?style=flat-square&logo=android-studio&logoColor=white)
![Html](http://img.shields.io/badge/-Html-e24c27?style=flat-square&logo=html5&logoColor=white)
![Css](http://img.shields.io/badge/-Css-2a65f1?style=flat-square&logo=css3&logoColor=white)
![Sass](http://img.shields.io/badge/-Sass-cc6699?style=flat-square&logo=sass&logoColor=white)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-8512fb?style=flat-square&logo=Bootstrap&logoColor=white)

![Mysql](http://img.shields.io/badge/-Mysql-white?style=flat-square&logo=mysql)

![Git](http://img.shields.io/badge/-Git-white?style=flat-square&logo=git)
![Npm](http://img.shields.io/badge/-Npm-white?style=flat-square&logo=npm&logoColor=white)

![VS Code](http://img.shields.io/badge/-VS%20Code-black?style=flat-square&logo=visualstudiocode&logoColor=3aa7f2)
![Sublime Text](http://img.shields.io/badge/-Sublime%20Text-484848?style=flat-square&logo=sublimetext)
